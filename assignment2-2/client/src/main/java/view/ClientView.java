package view;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import common.IServices;
import entities.Car;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class ClientView extends Application {

	public ClientView() {

	}

	@SuppressWarnings("restriction")
	@Override
	public void start(Stage stage) throws Exception {
		Text yearLabel = new Text("Enter fabrication year:");
		Text purchasePriceLabel = new Text("Enter purchase price:");
		Text engineLabel = new Text("Enter engine size:");
		Text resultLabel = new Text("Result:");

		TextField yearText = new TextField();
		TextField purchasePriceText = new TextField();
		TextField engineText = new TextField();

		TextField resultText = new TextField();

		Button buttonSellingPrice = new Button("Compute Selling price");
		Button buttonTax = new Button("Compute Tax");
		
		

		buttonSellingPrice.setOnMouseClicked((new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {

				System.out.println("price");
				try {
					System.setProperty("java.rmi.server.hostname","127.0.0.1");
					Registry registry = LocateRegistry.getRegistry(1098);
					
					IServices priceService = (IServices) registry.lookup("IServices");
					Car c = new Car(Integer.parseInt(yearText.getText()),
							Double.parseDouble(purchasePriceText.getText()), Integer.parseInt(engineText.getText()));
					double result = priceService.calculatePrice(c);
					System.out.println(result);
					resultText.setText(String.valueOf(result));
				} catch (RemoteException | NotBoundException e) {
					e.printStackTrace();
				}

			}
		}));

		buttonTax.setOnMouseClicked((new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				System.out.println("tax");
				try {
					Registry registry = LocateRegistry.getRegistry(1098);
					IServices taxService = (IServices) registry.lookup("IServices");
					Car c = new Car(Integer.parseInt(yearText.getText()),
							Double.parseDouble(purchasePriceText.getText()), Integer.parseInt(engineText.getText()));
					double result = taxService.calculteTax(c);
					System.out.println(result);
					resultText.setText(String.valueOf(result));
				} catch (RemoteException | NotBoundException e) {
					e.printStackTrace();
				}

			}
		}));

		GridPane gridPane = new GridPane();

		// Setting size for the pane
		gridPane.setMinSize(500, 500);

		// Setting the padding
		gridPane.setPadding(new Insets(10, 10, 10, 10));

		// Setting the vertical and horizontal gaps between the columns
		gridPane.setVgap(5);
		gridPane.setHgap(5);

		// Setting the Grid alignment
		gridPane.setAlignment(Pos.CENTER);

		gridPane.add(purchasePriceLabel, 0, 0);
		gridPane.add(purchasePriceText, 1, 0);

		gridPane.add(yearLabel, 0, 1);
		gridPane.add(yearText, 1, 1);

		gridPane.add(engineLabel, 0, 2);
		gridPane.add(engineText, 1, 2);

		gridPane.add(buttonSellingPrice, 0, 3);
		gridPane.add(buttonTax, 1, 3);

		gridPane.add(resultLabel, 0, 4);
		gridPane.add(resultText, 1, 4);

		gridPane.setStyle("-fx-background-color: BEIGE;");

		stage.setTitle("Car computations");

		// Creating a scene object
		Scene scene = new Scene(gridPane);

		// Adding scene to the stage
		stage.setScene(scene);

		// Displaying the contents of the stage
		stage.show();

	}

	public static void main(String args[]) {
		launch(args);
	}

}
