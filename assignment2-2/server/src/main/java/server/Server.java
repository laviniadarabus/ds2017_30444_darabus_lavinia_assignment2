package server;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import common.IServices;
import remote.ServicesImpl;

public class Server extends ServicesImpl {
	
	public Server() throws RemoteException{	
	}
	
	public static void main(String args[]) {
		try {
			System.setProperty("java.rmi.server.hostname","127.0.0.1");
			Registry registry = LocateRegistry.createRegistry(1098);
			
			
			
			ServicesImpl service = new ServicesImpl();
			IServices obj = (IServices) UnicastRemoteObject.exportObject(service, 0);
			
			registry.bind("IServices", obj);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		} catch (AlreadyBoundException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
	}

}
