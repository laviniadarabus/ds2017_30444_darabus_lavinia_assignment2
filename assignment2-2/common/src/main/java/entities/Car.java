package entities;

import java.io.Serializable;

public class Car implements Serializable{
	private int year;
	private int engineSize;
	private double price;
	
	public Car(int year, double price, int engineSize) {
		this.year = year;
		this.price = price;
		this.engineSize = engineSize;
	}

	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getEngineSize() {
		return engineSize;
	}
	public void setEngineSize(int engineSize) {
		this.engineSize = engineSize;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	

}
