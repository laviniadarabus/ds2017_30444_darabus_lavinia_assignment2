package common;

import java.rmi.Remote;
import java.rmi.RemoteException;

import entities.Car;

public interface IServices extends Remote {
	
	public double calculatePrice(Car car) throws RemoteException;
	public double calculteTax(Car car) throws RemoteException;

}
