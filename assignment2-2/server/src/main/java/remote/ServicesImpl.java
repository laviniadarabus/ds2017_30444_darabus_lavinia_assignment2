package remote;

import java.io.Serializable;

import common.IServices;
import entities.Car;

public class ServicesImpl implements IServices, Serializable{

	@Override
	public double calculatePrice(Car car) {
		if(car.getPrice()<=0){
			throw new IllegalArgumentException("Price must be positive");
		}
		return car.getPrice()- (car.getPrice()/7)*(2015-car.getYear());
	}

	@Override
	public double calculteTax(Car car) {
		int sum = 0;
		
		if (car.getEngineSize() <= 0) {
			throw new IllegalArgumentException("Engine size must be positive");
		}
		
		if (car.getEngineSize() < 1600) {
			sum = 8;
		}
		else {
			if(car.getEngineSize() >= 1601 && car.getEngineSize() <= 2000) {
				sum = 18;
			}
			else {
				if(car.getEngineSize() >= 2001 && car.getEngineSize() <= 2600) {
					sum = 72;
				}
				else {
					if(car.getEngineSize() >= 2601 && car.getEngineSize() <= 3000) {
						sum = 144;
					}
					else {
						sum = 290;
					}
				}
			}
		}
		return (double)(car.getEngineSize() / 200 * sum);
	}

}
